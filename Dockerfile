FROM debian

# Install the dhcpd server.
RUN apt update
RUN apt install -y isc-dhcp-server
RUN apt install -y net-tools

# Copy init script
RUN mkdir -p /data
COPY entrypoint.sh /data/entrypoint.sh
RUN chmod +x /data/entrypoint.sh

# Create a predefined dhcp configuration
RUN mkdir -p /data/dhcpd/
COPY dhcpd/dhcpd.conf /data/dhcpd/dhcpd.conf

# Client requests
EXPOSE 67/udp
USER root

ENTRYPOINT ["/data/entrypoint.sh"]