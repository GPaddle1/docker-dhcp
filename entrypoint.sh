#!/bin/sh

# Initialize the lease file if it doesn't exist.
touch /data/dhcpd/dhcpd.leases

# Setup eth0
ifconfig eth0 up
ifconfig eth0 192.168.16.1 netmask 255.255.255.0

# Start devpi-server.
dhcpd -cf /data/dhcpd/dhcpd.conf -lf /data/dhcpd/dhcpd.leases --no-pid -4 -f
