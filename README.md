# DHCP as a Service

## Utilisation

Ce répo est destiné à alimenter un conteneur Jenkins qui servira un service DHCP conteneurisé.
Il sera scruté par [ce répo](https://gitlab.com/GPaddle1/projet-systeme-docker)

## Structure

La structure actuelle se devra d'être respectée à savoir

```
.
├── Jenkinsfile
├── LICENSE
├── README.md
├── dhcpd
│   └── dhcpd.conf
├── docker-compose.yml
├── dockerfile
└── entrypoint.sh
```

Le Jenkinsfile pourra être modifié au besoin pour ajouter du test sur le fichier/le service DHCP

## Crédits

Ce répo se base sur [le code](https://github.com/SafeEval/docker-isc-dhcp-server) de [Jack Sullivan](https://github.com/SafeEval) 